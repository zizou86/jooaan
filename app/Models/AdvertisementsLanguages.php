<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdvertisementsLanguages extends Authenticatable
{

    use HasValidation;

    protected $table = 'advertisements_languages';
    public $timestamps = true;
    public $rules = [
        'language_id' => 'required',
        'table_id' => 'required',
        'name' => 'required',
        'description' => 'required',
    ];
    protected $guarded = ['id'];

    public function language()
    {
        return $this->belongsTo(Languages::class, 'language_id');
    }

}
