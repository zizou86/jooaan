<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Categories extends Authenticatable
{
    use HasValidation;
    use TraitLanguage;

    protected $table = "categories";
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $lang_columns = [
        'name',
        'created_at',
        'updated_at',
    ];

    public function scopeisActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeisUnActive($query)
    {
        return $query->where('is_active', false);
    }

}
