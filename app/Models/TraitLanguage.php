<?php

namespace App\Models;

trait TraitLanguage
{

    protected $forigen_key = 'table_id';
    protected $language_key = 'language_id';

    public function newQuery($excludeDeleted = true)
    {
        $builder = parent::newQuery($excludeDeleted);
        $builder->with('language');
        return $builder;
    }

    public function getAttribute($key)
    {
        if(in_array($key, $this->lang_columns))
        {
            if(is_object($this->language))
                return $this->language->$key;
            else
                return '';
        }
        else
            return parent::getAttribute($key);
    }

    function language()
    {
        return $this->hasOne(__CLASS__ . 'Languages', $this->forigen_key)
                ->where($this->language_key, $this->getLanguage());
    }

    function getLanguage()
    {
        $language = Languages::where('symbol', \App::getLocale())->first();
        return $language->id;
    }

}

?>