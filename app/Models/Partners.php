<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Partners extends Authenticatable
{
    use HasValidation;

    protected $table = "partners";
    protected $guarded = ['id'];
    public $timestamps = true;
    public $rules = [
        'ios_app_id' => 'required',
        'android_app_id' => 'required',
        'slug' => 'required|unique:partners',
        'name' => 'required',
        'logo' => 'required',
    ];

    public function countries()
    {
        return $this->hasMany(PartnersCountries::class, 'partner_id');
    }

    function getLogoUrlAttribute() {
        if ($this->logo != '')
            return url('uploads/' . $this->logo);
        else
            return url('uploads/images/avatar_image.png');
    }

}
