<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DistrictsLanguages extends Authenticatable
{

    use HasValidation;

    protected $table = 'districts_languages';
    public $timestamps = true;
    public $rules = [
        'language_id' => 'required',
        'table_id' => 'required',
        'name' => 'required',
    ];
    protected $guarded = ['id'];

    public function language()
    {
        return $this->belongsTo(Languages::class, 'language_id');
    }

}
