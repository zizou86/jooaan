<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Countries extends Authenticatable
{
    use HasValidation;
    use TraitLanguage;

    protected $table = "countries";
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $lang_columns = [
        'name',
        'created_at',
        'updated_at',
    ];
    public $rules = [
        'symbol' => 'required|unique:countries',
    ];

    public function cities()
    {
        return $this->hasMany(Cities::class, 'country_id')->whereIsActive(true);
    }

    public function scopeisActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeisUnActive($query)
    {
        return $query->where('is_active', false);
    }

}
