<?php

namespace App\Models;

use App\Models\HasValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cities extends Authenticatable
{
    use HasValidation;
    use TraitLanguage;

    protected $table = "cities";
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $lang_columns = [
        'name',
        'created_at',
        'updated_at',
    ];
    public $rules = [
        'country_id' => 'required',
    ];

    public function country()
    {
        return $this->belongsTo(Countries::class, 'country_id');
    }

    public function scopeisActive($query){
        return $query->where('is_active', true);
    }

    public function scopeisUnActive($query){
        return $query->where('is_active', false);
    }
   
}
