<?php

namespace App\Models;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

trait HasValidation
{

    public function validate($status = '')
    {
        $this->resolveUnique();
        if ($status = 'api') {
            $validator = Validator::make($this->toArray(), $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
        } else {
            Validator::make($this->toArray(), $this->rules)->validate();
        }
        return true;
    }

    private function resolveUnique()
    {
        foreach ($this->rules as $key => $value) {
            if (strstr($this->rules[$key], 'unique')) {
                $this->rules[$key] = preg_replace('/unique:([\w,]+)/i', '', $this->rules[$key]);
                $rules = explode('|', $this->rules[$key]);
                $rules[] = Rule::unique($this->table)->ignore($this->id);
                $this->rules[$key] = $rules;
            }
        }
    }

}
