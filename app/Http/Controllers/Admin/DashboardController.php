<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Stores;
use App\Models\Partners;
use App\Models\Countries;
use App\Models\Customers;
use App\Models\Languages;
use App\Models\Categories;
use App\Models\Advertisements;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $timeCache = $firstYear = Carbon::now()->addHour();
        // Cache::clear();

        $data = Cache::remember('dashboard', $timeCache, function () {
            $data['partners'] = Partners::count();
            $data['customers'] = Customers::count();
            $data['stores'] = Stores::count();
            $data['advertisements'] = Advertisements::count();
            $data['languages'] = Languages::count();
            $data['categories'] = Categories::count();
            $data['countries'] = Countries::count();
            $data['adminUsers'] = User::count();
            return $data;
        });

        return view('admin.statistics.dashboard')->with($data);
    }

}
