<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Customers'));
    }

    public function index(Request $request)
    {
        $data['result'] = Customers::orderBy('id', 'DESC');

        if ($request->filled('email')) {
            $data['result']->whereEmail($request->email);
        }

        if ($request->filled('phone')) {
            $data['result']->wherePhoneNumber($request->phone);
        }

        $data['result'] = $data['result']->paginate(10);

        return view('admin.content.customers.index')->with($data);
    }

    public function delete(Request $request, Customers $customer)
    {
        $customer->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
