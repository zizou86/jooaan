<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\ApiTokens;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Components\Helpers;

class ApiTokensController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.ApiTokens'));
    }

    public function index(Request $request)
    {
        $data['result'] = ApiTokens::orderBy('id', 'DESC');

        if ($request->filled('partner_id')) {
            $data['result']->wherePartnerId($request->partner_id);
        }

        $data['result'] = $data['result']->paginate(10);

        return view('admin.content.api_tokens.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.api_tokens.create')->with('apiToken', new ApiTokens);
    }

    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
            /**
             * Object Api Token
             */
            $apiToken = ApiTokens::findOrNew($request->api_token_id);
            $request->request->add(['token' => Helpers::genrateToken()]);
            $apiToken->fill($request->only(['partner_id', 'token']));
            if (!$apiToken->validate()) {
                $this->_errors = $apiToken->errors->all();
                throw new \Exception('Error', 133);
            }
            $apiToken->save();

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function activated(Request $request, ApiTokens $apiToken)
    {
        if ($apiToken->is_active == 0) {
            $apiToken->is_active = 1;
        } else {
            $apiToken->is_active = 0;
        }

        $apiToken->save();
    }

}
