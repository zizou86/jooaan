<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\CategoriesLanguages;
use DB;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Categories'));
    }

    public function index(Request $request)
    {
        $data['result'] = Categories::orderBy('id', 'DESC');

        if ($request->filled('name')) {
            $data['result']->join('categories_languages', 'categories_languages.table_id', 'categories.id')
                ->where('name', 'LIKE', "%{$request->name}%");
        }

        $data['result'] = $data['result']->select('categories.*')->paginate(10);

        return view('admin.content.categories.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.categories.create')->with('category', new Categories);
    }

    public function update(Request $request, Categories $category)
    {
        $categoryLanguage = CategoriesLanguages::whereTableId($category->id)->pluck('name', 'language_id')->toArray();

        return view('admin.content.categories.update')->with(['category' => $category, 'categoryLanguage' => $categoryLanguage]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Object Category
             */
            $category = Categories::findOrNew($request->category_id);
            $category->save();
            /**
             * Array Category Languages
             */
            foreach ($request->languages as $langauge_id => $name) {
                $categoryLanguage = CategoriesLanguages::firstOrNew(['table_id' => $category->id, 'language_id' => $langauge_id]);
                $categoryLanguage->name = $name;
                if (!$categoryLanguage->validate()) {
                    $this->_errors = $categoryLanguage->errors->all();
                    throw new \Exception('Error', 133);
                }
                $categoryLanguage->save();
            }

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function activated(Request $request, Categories $category)
    {
        if ($category->is_active == 0) {
            $category->is_active = 1;
        } else {
            $category->is_active = 0;
        }

        $category->save();
    }


    public function delete(Request $request, Categories $category)
    {
        $category->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
