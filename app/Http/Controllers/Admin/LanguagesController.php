<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Languages;
use DB;
use Illuminate\Http\Request;

class LanguagesController extends Controller
{

    protected $_errors;

    public function __CONSTRUCT()
    {
        view()->share('crudName', __('menu.Languages'));
    }

    public function index(Request $request)
    {
        $data['result'] = Languages::orderBy('id', 'DESC')->paginate(10);

        return view('admin.content.languages.index')->with($data);
    }

    public function create()
    {
        return view('admin.content.languages.create')->with('language', new Languages);
    }

    public function update(Request $request, Languages $language)
    {
        return view('admin.content.languages.update')->with('language', $language);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /**
             * Object Language
             */
            $language = Languages::findOrNew($request->language_id);
            $language->fill($request->only(['name', 'symbol', 'direction']));
            if (!$language->validate()) {
                $this->_errors = $language->errors->all();
                throw new \Exception('Error', 133);
            }
            $language->save();

            DB::commit();
            return redirect()->back()->with('success', __('partials.DataSavedSuccussfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($exception->getCode() == 133) {
                return redirect()->back()->with('error', $this->_errors);
            } else {
                dd($exception);
            }

        }
    }

    public function delete(Request $request, Languages $language)
    {
        $language->delete();
        $response = new \stdClass();
        $response->status = 'ok';
        $response->message = __('partials.DeletedSuccessfully');
        return response()->json($response);
    }
}
