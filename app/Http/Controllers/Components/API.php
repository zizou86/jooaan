<?php

namespace App\Http\Controllers\Components;

use App\Models\CitiesLanguages;
use App\Models\Countries;

class API
{

    public static function apiValidate($request, $array)
    {
        $missing = [];
        foreach ($array as $row) {
            if (!$request->has($row)) {
                $missing[$row] = $row . ' missing';
            }
        }
        if (!empty($missing)) {
            return response()->json(['status' => 0, 'message' => 'Error', 'data' => $missing], 200);
        }
    }

    public static function apiErrorMessage($errors, $code = 200)
    {
        if (is_array($errors)) {
            $errors = array_collapse($errors);
        }

        return response()->json(['status' => 0, 'message' => 'Error', 'data' => $errors], $code);
    }

    public static function apiSuccessMessage($data, $paginate = false, $code = 200)
    {
        if ($paginate == true) {
            $paginate = [
                'totalItems' => $data->total(),
                'hasMorePages' => $data->hasMorePages(),
                'nextPageUrl' => $data->nextPageUrl(),
            ];
            return response()->json(['status' => 1, 'message' => 'Success', 'paginate' => $paginate, 'data' => $data->items()], $code);
        }
        return response()->json(['status' => 1, 'message' => 'Success', 'data' => $data], $code);
    }

    public static function getCountry($request)
    {
        $ip = $request->ip();
        $data = ['current_country' => 0, 'current_city' => 0];
        if ($request->server('HTTP_HOST') != 'localhost') {
            $jsonData = file_get_contents('http://extreme-ip-lookup.com/json/' . $ip);
            $countryInfo = json_decode($jsonData, true);
            $country = Countries::where('symbol', $countryInfo['countryCode'])->first();
            $city = CitiesLanguages::where('name', $countryInfo['city'])->first();
            if (is_object($country) && is_object($city)) {
                $data = ['current_country' => $country->id, 'current_city' => $city->table_id];
            }
        }
        return $data;
    }

}
