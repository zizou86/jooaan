<?php

namespace App\Http\Middleware;

use App\Models\Languages;
use Closure;

class getLanguages
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('languages', Languages::all());
        return $next($request);
    }

}
