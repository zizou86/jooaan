<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Recommendations Language Lines
    |--------------------------------------------------------------------------
    */

    'DefaultData' => 'Default Data',
    'Advertise' => 'Advertise',
    'Customer' => 'Customer',
];
