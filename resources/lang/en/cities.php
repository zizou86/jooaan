<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cities Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New City',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Country' => 'Country',
    'Name' => 'Name',
    'PleaseEnterName' => 'Please Enter Name',
];
