<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Countries Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New Country',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Symbol' => 'Symbol',
    'Name' => 'Name',
    'PleaseEnterName' => 'Please Enter Name',
    'PleaseEnterSymbol' => 'Please Enter Symbol',
];
