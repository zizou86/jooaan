<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Customers Language Lines
    |--------------------------------------------------------------------------
    */

    'DefaultData' => 'Default Data',
    'Name' => 'Name',
    'Email' => 'Email',
    'PhoneNumber' => 'Phone Number',
    'PhoneType' => 'Phone Type',
    'Country' => 'Country',
    'PleaseEnterName' => 'Please Enter Name',
    'PleaseEnterEmail' => 'Please Enter Email',
];
