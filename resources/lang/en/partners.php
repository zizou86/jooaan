<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Partners Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New Partner',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Country' => 'Country',
    'Name' => 'Name',
    'IosAppId' => 'IOS App Id',
    'AndroidAppId' => 'Android App Id',
    'Logo' => 'Logo',
    'ColorCode' => 'Color Code',
    'PleaseEnterName' => 'Please Enter Name',
];
