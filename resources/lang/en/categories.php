<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Categories Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New Category',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Name' => 'Name',
    'PleaseEnterName' => 'Please Enter Name',
];
