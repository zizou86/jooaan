<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    */

    'Create' => 'Create New User',
    'Update' => 'Update',
    'DefaultData' => 'Default Data',
    'Name' => 'Name',
    'Email' => 'Email',
    'Password' => 'Password',
    'PleaseEnterName' => 'Please Enter Name',
    'PleaseEnterEmail' => 'Please Enter Email',
];
