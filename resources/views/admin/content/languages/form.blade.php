@push('css')
<link rel="stylesheet" type="text/css"
    href="{{asset('asset/app-assets/css/plugins/forms/validation/form-validation.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/vendors/css/forms/icheck/icheck.css')}}">
@endpush

<!-- Start From -->
<form class="form" action="{{route('languages.store')}}" method="POST" novalidate>
    @csrf
    <input type="hidden" name="language_id" value="{{$language->id}}">
    <div class="form-body">
        <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">{{__('languages.Name')}} <span class="danger">*</span></label>
                    <input type="text" name="name" value="{{$language->name}}" id="name" class="form-control"
                        placeholder="{{__('languages.Name')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="symbol">{{__('languages.Symbol')}} <span class="danger">*</span></label>
                    <input type="text" name="symbol" value="{{$language->symbol}}" id="symbol" class="form-control"
                        placeholder="{{__('languages.Symbol')}}" required
                        data-validation-required-message="{{__('partials.ThisFieldIsRequired')}}">
                </div>
            </div>

            <div class="col-md-4 skin skin-square">
                <label for="symbol">{{__('languages.Direction')}} <span class="danger">*</span></label>
                <div class="form-group">
                    <input type="radio" name="direction" value="ltr" @if($language->direction ==
                    'ltr') checked @endif id="input-radio-ltr">
                    <label for="input-radio-ltr">LTR</label>
                    &nbsp; &nbsp; &nbsp;
                    <input type="radio" name="direction" value="rtl" @if($language->direction ==
                    'rtl') checked @endif id="input-radio-rtl">
                    <label for="input-radio-rtl">RTL</label>
                </div>
            </div>

        </div>
    </div>

    <div class="form-actions right">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check-square-o"></i> {{__('partials.Save')}}
        </button>
    </div>
</form>


<!-- Start From -->

@push('js')
<script src="{{asset('asset/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript">
</script>

<script src="{{asset('asset/app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>

<script src="{{asset('asset//app-assets/js/scripts/forms/validation/form-validation.js')}}" type="text/javascript">
</script>
@endpush