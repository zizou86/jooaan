@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/css/plugins/forms/wizard.css')}}">
@endpush

<!-- Start From -->
<form action="{{route('districts.store')}}" method="POST" class="steps-validation wizard-circle">
    @csrf
    <input type="hidden" name="district_id" value="{{$district->id}}">
    <!-- Default Data -->
    <h6>{{__('districts.DefaultData')}}</h6>
    <fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!!\App\Http\Controllers\Components\Dropdown::getCountries(@$district->city->country_id,
                    'required')!!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!!\App\Http\Controllers\Components\Dropdown::getCities(@$district->city->country_id,
                    $district->city_id, 'required')!!}
                </div>
            </div>
        </div>
    </fieldset>

    <!-- Languages -->
    @foreach ($languages as $language)
    <h6>{{$language->name}}</h6>
    <fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">
                        {{__('districts.Name')}}
                        <span class="danger">*</span>
                    </label>
                    <input type="text" name="languages[{{$language->id}}]" value="{{@$districtLanguage[$language->id]}}"
                        class="form-control required" id="name">
                </div>
            </div>
        </div>
    </fieldset>
    @endforeach
</form>
<!-- Start From -->

@push('js')
<script src="{{asset('asset/app-assets/vendors/js/extensions/jquery.steps.min.js')}}" type="text/javascript"></script>

<script src="{{asset('asset/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}" type="text/javascript">
</script>

@include('admin.partials.wizard_form_script')
<script>
    $('select[name="country_id"]').on('change',function(e){
        let country_id = $(this).val();
        $.ajax({
            method:'GET',
            url:'{{url("get-cities/")}}/'+country_id,
            dataType:'json',
            success: function(response){
                $('select[name="city_id"]').empty();
                $.each(response, function(val, text) {
                    $('select[name="city_id"]')
                        .append($("<option></option>")
                        .attr("value", text.id)
                        .text(text.name));
                });
            }

        });
    });
</script>
@endpush