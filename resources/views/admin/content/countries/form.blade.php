@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('asset/app-assets/css/plugins/forms/wizard.css')}}">
@endpush

<!-- Start From -->
<form action="{{route('countries.store')}}" method="POST" class="steps-validation wizard-circle">
    @csrf
    <input type="hidden" name="country_id" value="{{$country->id}}">
    <!-- Default Data -->
    <h6>{{__('countries.DefaultData')}}</h6>
    <fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="symbol">
                        {{__('countries.Symbol')}}
                        <span class="danger">*</span>
                    </label>
                    <input type="text" name="symbol" value="{{$country->symbol}}" class="form-control required"
                        id="symbol">
                </div>
            </div>
        </div>
    </fieldset>

    <!-- Languages -->
    @foreach ($languages as $language)
    <h6>{{$language->name}}</h6>
    <fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">
                        {{__('countries.Name')}}
                        <span class="danger">*</span>
                    </label>
                    <input type="text" name="languages[{{$language->id}}]" value="{{@$countryLanguage[$language->id]}}"
                        class="form-control required" id="name">
                </div>
            </div>
        </div>
    </fieldset>
    @endforeach
</form>
<!-- Start From -->

@push('js')
<script src="{{asset('asset/app-assets/vendors/js/extensions/jquery.steps.min.js')}}" type="text/javascript"></script>

<script src="{{asset('asset/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}" type="text/javascript">
</script>

@include('admin.partials.wizard_form_script')
@endpush