@extends('admin.partials.layout')
@section('title', __('menu.Dashboard'))

@section('page-header')
@include('admin.partials.page-header',[
'pageTitle'=> __('menu.Dashboard'),
'pagesBreadcrumb'=>[],
'currentPageTitle'=> __('menu.Dashboard'),
])
@endsection

@section('content')
<div class="row">
  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-info">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-briefcase text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($partners)}}</h3>
              <span><a href="{{route('partners')}}" class="text-white">{{__('menu.Partners')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-danger">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-users text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($customers)}}</h3>
              <span><a href="{{route('customers')}}" class="text-white">{{__('menu.Customers')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-success">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-shopping-cart text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($stores)}}</h3>
              <span><a href="{{route('stores')}}" class="text-white">{{__('menu.Stores')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-warning">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-bullhorn text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($advertisements)}}</h3>
              <span><a href="{{route('advertisements')}}" class="text-white">{{__('menu.Advertisements')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-warning">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-language text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($languages)}}</h3>
              <span><a href="{{route('languages')}}" class="text-white">{{__('menu.Languages')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-danger">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-th-large text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($categories)}}</h3>
              <span><a href="{{route('categories')}}" class="text-white">{{__('menu.Categories')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-success">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-globe text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($countries)}}</h3>
              <span><a href="{{route('countries')}}" class="text-white">{{__('menu.Countries')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-lg-6 col-12">
    <div class="card bg-gradient-directional-info">
      <div class="card-content">
        <div class="card-body">
          <div class="media d-flex">
            <div class="align-self-center">
              <i class="fa fa-user-secret text-white font-large-2 float-left"></i>
            </div>
            <div class="media-body text-white text-right">
              <h3 class="text-white">{{number_format($adminUsers)}}</h3>
              <span><a href="{{route('adminUsers')}}" class="text-white">{{__('menu.AdminUsers')}}</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection