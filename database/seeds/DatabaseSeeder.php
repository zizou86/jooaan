<?php

use App\Models\Stores;
use App\Models\Partners;
use App\Models\ApiTokens;
use App\Models\Countries;
use App\Models\Customers;
use App\Models\Languages;
use App\Models\Categories;
use Illuminate\Support\Str;
use App\Models\Advertisements;
use App\Models\Recommendations;
use App\Models\StoresLanguages;
use Illuminate\Database\Seeder;
use App\Models\PartnersCountries;
use App\Models\CategoriesLanguages;
use App\Models\AdvertisementsLanguages;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguagesTableSeeder::class);
        $this->call(WorldDataTableSeeder::class);
        $this->call(UserTableSeeder::class);
        factory(Partners::class, 1000)->create()->each(function ($partner) {
            PartnersCountries::create(['partner_id' => $partner->id, 'country_id' => Countries::inRandomOrder()->first()->id]);
        });
        factory(ApiTokens::class, 1000)->create();
        factory(Categories::class, 100)->create()->each(function ($store) {
            foreach (Languages::all() as $language) {
                CategoriesLanguages::create(['table_id' => $store->id, 'language_id' => $language->id, 'name' => 'category name-' . Str::random(5) . '-' . $language->name]);
            }
        });
        factory(Stores::class, 1000)->create()->each(function ($store) {
            foreach (Languages::all() as $language) {
                StoresLanguages::create(['table_id' => $store->id, 'language_id' => $language->id, 'name' => 'store name-' . Str::random(5) . '-' . $language->name]);
            }
        });
        factory(Advertisements::class, 1000)->create()->each(function ($store) {
            foreach (Languages::all() as $language) {
                AdvertisementsLanguages::create(['table_id' => $store->id, 'language_id' => $language->id, 'name' => 'category name-' . Str::random(5) . '-' . $language->name, 'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"]);
            }
        });
        factory(Customers::class, 1000)->create()->each(function ($customer) {
            for ($i = 0; $i <= rand(2, 30); $i++) {
                Recommendations::create(['advertise_id' => Advertisements::inRandomOrder()->first()->id, 'customer_id' => $customer->id]);
            }
        });
    }
}
