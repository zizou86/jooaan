<?php

use App\Models\Languages;
use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            [
                'name' => 'English',
                'symbol' => 'en',
                'direction' => 'ltr',
            ],
            [
                'name' => 'Arabic',
                'symbol' => 'ar',
                'direction' => 'rtl',
            ],
        ];

        foreach ($languages as $row) {
            Languages::create($row);
        }
    }
}
