<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdvertisementsLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('advertisements_languages', function(Blueprint $table)
		{
			$table->foreign('table_id', 'advertisements_languages_ibfk_1')->references('id')->on('advertisements')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'advertisements_languages_ibfk_2')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('advertisements_languages', function(Blueprint $table)
		{
			$table->dropForeign('advertisements_languages_ibfk_1');
			$table->dropForeign('advertisements_languages_ibfk_2');
		});
	}

}
