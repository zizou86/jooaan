<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDistrictsLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('districts_languages', function(Blueprint $table)
		{
			$table->foreign('table_id', 'districts_languages_ibfk_1')->references('id')->on('districts')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'districts_languages_ibfk_2')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('districts_languages', function(Blueprint $table)
		{
			$table->dropForeign('districts_languages_ibfk_1');
			$table->dropForeign('districts_languages_ibfk_2');
		});
	}

}
