<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecommendationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recommendations', function(Blueprint $table)
		{
			$table->foreign('advertise_id', 'recommendations_ibfk_1')->references('id')->on('advertisements')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('customer_id', 'recommendations_ibfk_2')->references('id')->on('customers')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recommendations', function(Blueprint $table)
		{
			$table->dropForeign('recommendations_ibfk_1');
			$table->dropForeign('recommendations_ibfk_2');
		});
	}

}
