<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPartnersCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partners_countries', function(Blueprint $table)
		{
			$table->foreign('partner_id', 'partners_countries_ibfk_1')->references('id')->on('partners')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('country_id', 'partners_countries_ibfk_2')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partners_countries', function(Blueprint $table)
		{
			$table->dropForeign('partners_countries_ibfk_1');
			$table->dropForeign('partners_countries_ibfk_2');
		});
	}

}
