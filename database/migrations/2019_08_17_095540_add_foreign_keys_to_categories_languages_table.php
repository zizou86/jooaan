<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCategoriesLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories_languages', function(Blueprint $table)
		{
			$table->foreign('table_id', 'categories_languages_ibfk_1')->references('id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'categories_languages_ibfk_2')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categories_languages', function(Blueprint $table)
		{
			$table->dropForeign('categories_languages_ibfk_1');
			$table->dropForeign('categories_languages_ibfk_2');
		});
	}

}
