<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries_languages', function(Blueprint $table)
		{
			$table->foreign('table_id', 'countries_languages_ibfk_1')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('language_id', 'countries_languages_ibfk_2')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries_languages', function(Blueprint $table)
		{
			$table->dropForeign('countries_languages_ibfk_1');
			$table->dropForeign('countries_languages_ibfk_2');
		});
	}

}
