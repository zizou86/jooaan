<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Http\Controllers\Components\Helpers;
use App\Models\Countries;
use App\Models\Stores;
use Faker\Generator as Faker;

$factory->define(Stores::class, function (Faker $faker) {
    return [
        'country_id' => function () {
            return Countries::inRandomOrder()->first()->id;
        },
        'slug' => str_slug($faker->sentence),
        'logo' => $faker->image('public/uploads/',400,300, 'food', false),
    ];
});
