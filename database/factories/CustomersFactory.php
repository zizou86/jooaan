<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Countries;
use App\Models\Customers;
use Illuminate\Support\Arr;
use Faker\Generator as Faker;

$parent = $factory->define(Customers::class, function (Faker $faker) {
    $array = ['Android', 'Iphone'];
    $randomItems = Arr::random($array, 1);
    return [
        'country_id' => function () {
            return Countries::inRandomOrder()->first()->id;
        },
        'name' => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'phone_type' => $randomItems[0],
        'email' => $faker->email,
        'push_token' => str_random(50),
    ];
});
