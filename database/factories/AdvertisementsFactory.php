<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Advertisements;
use App\Models\Categories;
use App\Models\Cities;
use App\Models\Partners;
use App\Models\Stores;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Vinkla\Hashids\Facades\Hashids;

$factory->define(Advertisements::class, function (Faker $faker) {
    $typeArray = [1, 2, 3];
    $typeId = Arr::random($typeArray, 1);
    $valueTypeArray = [0, 1];
    $valueTypeId = Arr::random($valueTypeArray, 1);
    $startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp());
    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addDays(rand(1, 150));
    return [
        'hash_id' => Hashids::encode(rand(9999, 999999999)),
        'type_id' => $typeId[0],
        'partner_id' => function () {
            return Partners::inRandomOrder()->first()->id;
        },
        'city_id' => function () {
            return Cities::inRandomOrder()->first()->id;
        },
        'store_id' => function () {
            return Stores::inRandomOrder()->first()->id;
        },
        'category_id' => function () {
            return Categories::inRandomOrder()->first()->id;
        },
        'code' => str_random(10),
        'value' => $faker->randomFloat(2, 1, 1000),
        'value_type' => $valueTypeId[0],
        'start_at' => $startDate,
        'end_at' => $endDate,
    ];
});
