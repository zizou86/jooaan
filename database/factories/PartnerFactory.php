<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Partners;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Partners::class, function (Faker $faker) {
    return [
        'ios_app_id' => Str::random(50),
        'android_app_id' => Str::random(50),
        'slug' => str_slug($faker->sentence),
        'name' => $faker->name,
        'logo' => $faker->image('public/uploads/',400,300, 'business', false),
        'color_code' => $faker->colorName,
    ];
});
