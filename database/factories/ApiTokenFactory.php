<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Http\Controllers\Components\Helpers;
use App\Models\ApiTokens;
use App\Models\Partners;
use Faker\Generator as Faker;

$factory->define(ApiTokens::class, function (Faker $faker) {
    return [
        'partner_id' => function () {
            return Partners::inRandomOrder()->first()->id;
        },
        'token' => Helpers::genrateToken(),
        'is_active' => 1,
    ];
});
